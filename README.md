# DRAFTER

A drafting module for StreetRaces.

Big thanks to Fingerguns for collaborating with me on bits and pieces of the code, and Jutzy for dumping it on his dev server for testing in an environment closer to the server its intended for.

## Update

Big ups to trying natives again. Its all natives now.


## Installation

Choose your version. Both are available in [Releases](https://gitlab.com/hattiehats/drafter-fivem/-/releases).
- **Custom** is heavier and more machine intensive, but is extremely customisable.
- **Natives-Only** Exclusively uses natives to operate. Its tiny, and you can just drop it in.

Standard fiveM installation for resources. Dump these files in a folder in your server resources folder. You may need to double-check your dependencies and compare them to the script name you're using.

Custom will require some setup, check its README.