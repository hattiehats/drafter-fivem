-- Race var. Literally all we need rn.
race = 0

-- Init, just to make sure nothing dumb happens
SetEnableVehicleSlipstreaming(false)

-- EVENT HANDLERS
RegisterNetEvent("StreetRaces:joinedRace_cl")
AddEventHandler("StreetRaces:joinedRace_cl", function(index)
    race = index
    SetEnableVehicleSlipstreaming(true)
end)

RegisterNetEvent("StreetRaces:removeRace_cl")
AddEventHandler("StreetRaces:removeRace_cl", function(index)
    race = 0
    SetEnableVehicleSlipstreaming(index == race)
end)